import java.util.Scanner;

public class Insurance {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		// For Age
		System.out.print("Enter age : ");
		String input = scanner.nextLine();
		int age = 0;
		float basePremium = 0;
		try {
			age = Integer.parseInt(input);
			basePremium = premiumBasedOnAge(age);
		} catch (Exception e) {
			System.out.println("Age Entered is not an integer!!");
		}

		// Gender Rule
		System.out.print("Is Gender Male(true/false) : ");
		boolean gender = false;
		try {
			gender = scanner.nextBoolean();
		} catch (Exception e) {
			System.out.print("Enter True or False");
			// System.exit(0);
		}

		if (gender) {
			basePremium =  (float) (basePremium + (0.02 * basePremium));

		}

		// Health condition
		float premiumAfterHealth = premiumBasedOnHealth(basePremium, scanner);

		// Habits
		float premiumAfterHabits = premiumBasedOnHabits(premiumAfterHealth, scanner);

		System.out.println("Total Premium Amount : " + Math.ceil(premiumAfterHabits));

	}

	private static float premiumBasedOnHabits(float premiumAfterHealth, Scanner scanner) {
		float badHabbitPremium = 0;
		System.out.print("Smoking (true/false) : ");
		boolean smoking = false;
		try {
			smoking = scanner.nextBoolean();
		} catch (Exception e) {
			System.out.print("Enter True or False");
		}
		if (smoking) {
			badHabbitPremium = badHabbitPremium + (float) ((0.03 * premiumAfterHealth));

		}

		float goodHabbitPremium = 0;
		System.out.print("Daily Exercise (true/false) : ");
		boolean dailyexercise = false;
		try {
			dailyexercise = scanner.nextBoolean();
		} catch (Exception e) {
			System.out.print("Enter True or False");
		}
		if (dailyexercise) {
			goodHabbitPremium = goodHabbitPremium + (int) ((0.03 * premiumAfterHealth));

		}

		System.out.print("Alcohol (true/false) : ");
		boolean alcohol = false;
		try {
			alcohol = scanner.nextBoolean();
		} catch (Exception e) {
			System.out.print("Enter True or False");
		}
		if (alcohol) {
			badHabbitPremium = badHabbitPremium + (int) ((0.03 * premiumAfterHealth));

		}

		System.out.print("Drugs (true/false) : ");
		boolean drugs = false;
		try {
			drugs = scanner.nextBoolean();
		} catch (Exception e) {
			System.out.print("Enter True or False");
		}
		if (drugs) {
			badHabbitPremium = badHabbitPremium + (int) ((0.03 * premiumAfterHealth));

		}

		System.out.println(premiumAfterHealth + badHabbitPremium - goodHabbitPremium);
		return premiumAfterHealth + badHabbitPremium - goodHabbitPremium;
	}

	private static float premiumBasedOnHealth(float basePremium, Scanner scanner) {
		System.out.print("Have Hypertension (true/false) : ");
		boolean hypertension = false;
		try {
			hypertension = scanner.nextBoolean();
		} catch (Exception e) {
			System.out.print("Enter True or False");
		}
		if (hypertension) {
			basePremium =  (float) (basePremium + (0.01 * basePremium));

		}

		System.out.print("Have Blood pressure (true/false) : ");
		boolean bloodpressure = false;
		try {
			bloodpressure = scanner.nextBoolean();
		} catch (Exception e) {
			System.out.print("Enter True or False");
		}
		if (bloodpressure) {
			basePremium = (float) (basePremium + (0.01 * basePremium));

		}

		System.out.print("Have Blood sugar (true/false) : ");
		boolean bloodsugar = false;
		try {
			bloodsugar = scanner.nextBoolean();
		} catch (Exception e) {
			System.out.print("Enter True or False");
		}
		if (bloodsugar) {
			basePremium = (float) (basePremium + (0.01 * basePremium));

		}

		System.out.print("Have Overweight (true/false) : ");
		boolean overweight = false;
		try {
			overweight = scanner.nextBoolean();
		} catch (Exception e) {
			System.out.print("Enter True or False");
		}
		if (overweight) {
			basePremium = (float) (basePremium + (0.01 * basePremium));

		}

		System.out.println("Premium based on Health Condition " + basePremium);
		return basePremium;
	}

	private static float premiumBasedOnAge(int age) {

		float premiumAmount = 0;
		float basePremium = 5000;

		if (age > 18 && age <= 40) {
			int temp = (age - 18) / 5;
			for (int i = 1; i <= temp; i++) {
				premiumAmount = (float) (((0.1 * basePremium)));
				basePremium = basePremium + premiumAmount;
			}

		} else if (age > 40) {
			int temp = (age - 18) / 5;
			for (int i = 1; i <= temp; i++) {
				premiumAmount = (int) (((0.2 * basePremium)));
				basePremium = basePremium + premiumAmount;
			}

			System.out.println(basePremium);
		} else {
			basePremium = 5000;
		}
		System.out.println(basePremium);
		return basePremium;
	}

}
