package Test;

import org.junit.Before;
import org.junit.Test;

public class InsuranceTest {

	int age;
	float basePremium;
	boolean hypertension;
	boolean bloodpressure;
	boolean bloodsugar;
	boolean overweight;
	float badHabbitPremium;
	float goodHabbitPremium;
	boolean smoking;
	boolean dailyexercise;
	boolean drugs;
	boolean alcohol;
	float premiumAfterHealth;

	@Before
	public void setdata() {
		age = 34;
		basePremium = 5000f;
		hypertension = false;
		bloodpressure = false;
		bloodsugar = false;
		overweight = true;
		premiumAfterHealth = basePremium;
		badHabbitPremium = 0;
		goodHabbitPremium = 0;
		smoking = false;
		dailyexercise = true;
		alcohol = true;
		drugs = false;
	}

	@Test
	public void premiumBasedOnAge() {

		float premiumAmount = 0;
		float basePremium = 5000;

		if (age > 18 && age <= 40) {
			int temp = (age - 18) / 5;
			for (int i = 1; i <= temp; i++) {
				premiumAmount = (float) (((0.1 * basePremium)));
				basePremium = basePremium + premiumAmount;
			}

		} else if (age > 40) {
			int temp = (age - 18) / 5;
			for (int i = 1; i <= temp; i++) {
				premiumAmount = (int) (((0.2 * basePremium)));
				basePremium = basePremium + premiumAmount;
			}

			System.out.println(basePremium);
		} else {
			basePremium = 5000;
		}
		System.out.println(basePremium);
	}

	@Test
	public void premiumBasedOnHealth() {
		if (hypertension) {
			basePremium = (float) (basePremium + (0.01 * basePremium));

		}

		if (bloodpressure) {
			basePremium = (float) (basePremium + (0.01 * basePremium));

		}

		if (bloodsugar) {
			basePremium = (float) (basePremium + (0.01 * basePremium));

		}

		if (overweight) {
			basePremium = (float) (basePremium + (0.01 * basePremium));

		}

		System.out.println("Premium based on Health Condition " + basePremium);
	}

	@Test
	public void premiumBasedOnHabits() {

		if (smoking) {
			badHabbitPremium = badHabbitPremium + (float) ((0.03 * premiumAfterHealth));

		}

		if (dailyexercise) {
			goodHabbitPremium = goodHabbitPremium + (int) ((0.03 * premiumAfterHealth));

		}

		if (alcohol) {
			badHabbitPremium = badHabbitPremium + (int) ((0.03 * premiumAfterHealth));

		}

		if (drugs) {
			badHabbitPremium = badHabbitPremium + (int) ((0.03 * premiumAfterHealth));

		}

		System.out.println(premiumAfterHealth + badHabbitPremium - goodHabbitPremium);
	}

}
